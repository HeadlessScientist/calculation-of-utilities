# Программа расчета комунальных услуг
### Программа состоит из микросервиса и графического интерфейса wpf
### Микросервис представляет из себя TCP сервер
## Важно! Сначала запустить сервер
### Запуск сервера выполняется командами:
### cd .\calculation-of-utilities\app\
### dotnet run
### Запуск клиента выполняется командами:
### cd .\calculation-of-utilities\gui\
### dotnet run