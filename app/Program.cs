﻿using calculation;
using server;
using database;

namespace COU
{
    class Program
    {
        static void Main(string[] args)
        {
            DB db = new DB();
            try
            {
                db.CreateTable();
                Console.WriteLine("Созданна новая бд");
            } catch {
                Console.WriteLine("Подключенно к существующей бд");
            }
            TCPserver app = new TCPserver(db);
            while (true){}
        }
    }
}