namespace calculation
{
    class ColdWaterCalculation : GenCalculation
    {
        public static float CapacityWater(float current_indication, float start_indication = 0)
        {
            return Capacity(current_indication, start_indication);
        }

        public static float CapacityWater(int human, float standart)
        {
            return Capacity(human, standart);
        }

        public static float CalcColdWater(float water_tarrif, float water)
        {
            // Расчет стоимости холодной воды
            return water_tarrif * water;
        }
    }
}