namespace calculation
{
    class EnergyCalculation : GenCalculation
    {
        public static float CapacityEnergy(float current_indication, float start_indication = 0)
        {
            return Capacity(current_indication, start_indication);
        }

        public static float CapacityEnergy(int human, float standart)
        {
            return Capacity(human, standart);
        }

        public static float CalcEnergy(float energy_tarrif, float energy)
        {
            // Расчет стоимости энергии
            return energy_tarrif * energy;
        }
    }
}