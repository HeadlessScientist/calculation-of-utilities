namespace calculation
{
    class GenCalculation
    {
        protected static float GeneralCalculation(float capacity, float tariff)
        {
            // Общий алгоритм расчета
            return capacity * tariff;
        }

        protected static float Capacity(float current_indication, float start_indication = 0)
        {
            // Алгоритм расчета по показаниям приборов учета
            return current_indication - start_indication;
        }

        protected static float Capacity(int humans, float tariff_for_human)
        {
            // Алгоритм расчета по нормативному объему
            return Convert.ToInt32(humans) * tariff_for_human;
        }
    }
}