namespace calculation
{
    class HotWaterCalculation : ColdWaterCalculation
    {
        public static float CalcTermalEnergy(float energy_tarrif, float standart, float water)
        {
            // Расчет стоимости тепловой энергии
            return energy_tarrif * water * standart;
        }

        public static float CalcHotWater(float water_tarrif, float current_indication, float start_indication = 0)
        {
            // Получение стоимости воды и тепловой энергии для ее нагрева
            return CalcColdWater(water_tarrif, CapacityWater(current_indication, start_indication));
        }

        public static float CalcHotWater(float water_tarrif, int human, float standart)
        {
            // Получение стоимости воды и тепловой энергии для ее нагрева
            return CalcColdWater(water_tarrif, CapacityWater(human, standart));
        }
    }
}