using System.Text.Json;
using dataclasses;
using database;

namespace server
{
    static class Route
    {
        static private Dictionary<string, IView> routes = new Dictionary<string, IView>()
        {
            ["CalculatePrice"] = new CalculatePrice()
        };
        public static string? CallMethod(string data, DB db)
        {
            Method? method = JsonSerializer.Deserialize<Method>(data);

            foreach (var view in routes)
            {
                if (view.Key == method.method)
                {
                    view.Value.Request(method.param);
                    return view.Value.Responce(db);
                }
            }
            return null;
        }
    }
}