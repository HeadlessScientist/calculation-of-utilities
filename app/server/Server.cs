using System.Net.Sockets;
using System;
using System.IO;
using System.Text;
using System.Net;
using System.Threading.Tasks;
using database;

namespace server
{
    class TCPserver
    {
        private TcpListener listener;

        private DB db;

        public TCPserver(DB db, int port = 8080)
        {
            this.db = db;
            listener = new TcpListener(IPAddress.Any, port);
            listener.Start();
            Console.WriteLine("TCP server start");
            GetClient();
        }

        async private void GetClient()
        {
            while (true)
            {
                using TcpClient? tcpClient = await listener.AcceptTcpClientAsync();
                if (tcpClient != null)
                {
                    Console.WriteLine($"Клиент {tcpClient.Client.RemoteEndPoint} подключился");
                    try
                    {
                        await Send(tcpClient);
                    } catch {
                        Console.WriteLine($"Клиент {tcpClient.Client.RemoteEndPoint} отключился");
                    }
                }
            }
        }

        async private Task Send(TcpClient client)
        {
            var stream = client.GetStream();
            byte[] data = new byte[1024];
            string raw_response;
            int msg;
            string request;

            while (true){
                msg = await stream.ReadAsync(data);
                if (msg > 0)
                {
                    request = Encoding.UTF8.GetString(data, 0, msg);
                    raw_response = Route.CallMethod(request, db);
                    byte[] response = Encoding.UTF8.GetBytes(raw_response);
                    await stream.WriteAsync(response);
                }
            }
        }
    }
}