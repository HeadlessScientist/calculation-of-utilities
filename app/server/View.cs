using System.Text.Json;
using dataclasses;
using calculation;
using database;

namespace server
{   
    interface IView
    {
        public void Request(string param, DB? db = null);
        public string Responce(DB? db = null);
    }
    class CalculatePrice: IView
    {
        private GetDiviceData? data;
        public void Request(string param, DB? db = null)
        {
            data = JsonSerializer.Deserialize<GetDiviceData>(param);
        }

        public string Responce(DB? db = null)
        {
            float cold_water;
            float hot_water;
            float hot_water_energy;
            float energy = 0;
            float energy_day = 0;
            float energy_night = 0;
            float final_price;

            if (data.check_divices.cold_water)
            {
                cold_water = ColdWaterCalculation.CalcColdWater(
                    data.tariff.cold_water,
                    ColdWaterCalculation.CapacityWater(
                        data.cold_water.current,
                        data.cold_water.start
                    )
                );
            } else {
                cold_water = ColdWaterCalculation.CalcColdWater(
                    data.tariff.cold_water,
                    ColdWaterCalculation.CapacityWater(
                        data.human,
                        data.standart.cold_water
                    )
                );
            }

            if (data.check_divices.hot_water)
            {
                hot_water = HotWaterCalculation.CalcHotWater(
                    data.tariff.hot_water,
                    data.hot_water.current,
                    data.hot_water.start
                );
                hot_water_energy = HotWaterCalculation.CalcTermalEnergy(
                    data.tariff.hot_water_energy,
                    data.standart.hot_water_energy,
                    HotWaterCalculation.CapacityWater(
                        data.hot_water.current,
                        data.hot_water.start
                    )
                );
            } else {
                hot_water = HotWaterCalculation.CalcHotWater(
                    data.tariff.hot_water,
                    data.human,
                    data.standart.hot_water
                );
                hot_water_energy = HotWaterCalculation.CalcTermalEnergy(
                    data.tariff.hot_water_energy,
                    data.standart.hot_water_energy,
                    HotWaterCalculation.CapacityWater(
                        data.human,
                        data.standart.hot_water
                    )
                );
            }

            if (data.check_divices.energy)
            {
                energy_day = EnergyCalculation.CalcEnergy(
                    data.tariff.energy_day,
                    EnergyCalculation.CapacityEnergy(
                        data.energy_day.current,
                        data.energy_day.start
                    )
                );
                energy_night = EnergyCalculation.CalcEnergy(
                    data.tariff.energy_night,
                    EnergyCalculation.CapacityEnergy(
                        data.energy_night.current,
                        data.energy_night.start
                    )
                );
            } else {
                energy = EnergyCalculation.CalcEnergy(
                    data.tariff.energy,
                    EnergyCalculation.CapacityEnergy(
                        data.human,
                        data.standart.energy
                    )
                );
            }

            final_price = cold_water + hot_water + hot_water_energy + energy + energy_day + energy_night;

            ResponseCalculatePrice response = new ResponseCalculatePrice(
                cold_water,
                hot_water,
                hot_water_energy,
                final_price,
                energy,
                energy_day,
                energy_night
            );

            db.InsertIntoCalculation(response);

            return JsonSerializer.Serialize(response);
        }
    }
}