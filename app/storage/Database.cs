using System;
using Microsoft.Data.Sqlite;
using dataclasses;

namespace database
{
    class DBCore
    {
        protected SqliteConnection connection;
        public DBCore()
        {
            connection = new SqliteConnection("Data Source=db.db");
            connection.Open();
        }
    }

    class DB : DBCore
    {
        public void CreateTable()
        {
            SqliteCommand command = new SqliteCommand();
            command.Connection = connection;
            command.CommandText = """
                CREATE TABLE Calculation(
                    _id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
                    data TEXT NOT NULL,
                    cold_water real NOT NULL,
                    hot_water real NOT NULL,
                    hot_water_energy real NOT NULL,
                    energy_day real,
                    energy_night real,
                    energy real,
                    final_price real NOT NULL
                )
            """;
            command.ExecuteNonQuery();
        }

        public void InsertIntoCalculation(ResponseCalculatePrice values)
        {
            SqliteCommand command = new SqliteCommand();
            command.Connection = connection;
            command.CommandText = $"""
                INSERT INTO Calculation (
                        data,
                        cold_water,
                        hot_water,
                        hot_water_energy,
                        energy_day,
                        energy_night,
                        energy,
                        final_price
                    )
                VALUES (
                        @date,
                        @cold_water,
                        @hot_water,
                        @hot_water_energy,
                        @energy_day,
                        @energy_night,
                        @energy,
                        @final_price
                    );
            """;

            SqliteParameter date = new SqliteParameter("@date", SqliteType.Text);
            date.Value = DateTime.Today;
            SqliteParameter cold_water = new SqliteParameter("@cold_water", SqliteType.Real);
            cold_water.Value = values.cold_water;
            SqliteParameter hot_water = new SqliteParameter("@hot_water", SqliteType.Real);
            hot_water.Value = values.hot_water;
            SqliteParameter hot_water_energy = new SqliteParameter("@hot_water_energy", SqliteType.Real);
            hot_water_energy.Value = values.hot_water_energy;
            SqliteParameter energy_day = new SqliteParameter("@energy_day", SqliteType.Real);
            energy_day.Value = values.energy_day;
            SqliteParameter energy_night = new SqliteParameter("@energy_night", SqliteType.Real);
            energy_night.Value = values.energy_night;
            SqliteParameter energy = new SqliteParameter("@energy", SqliteType.Real);
            energy.Value = values.energy;
            SqliteParameter final_price = new SqliteParameter("@final_price", SqliteType.Real);
            final_price.Value = values.final_price;

            command.Parameters.Add(date);
            command.Parameters.Add(cold_water);
            command.Parameters.Add(hot_water);
            command.Parameters.Add(hot_water_energy);
            command.Parameters.Add(energy_day);
            command.Parameters.Add(energy_night);
            command.Parameters.Add(energy);
            command.Parameters.Add(final_price);

            command.ExecuteNonQuery();
        }
    }
}