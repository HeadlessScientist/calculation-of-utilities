﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using client;
using config;

namespace gui
{
    public partial class MainWindow : Window
    {
        public TCPClient client = new TCPClient();
        public Config config = new Config();

        public MainWindow()
        {
            CreateClient();
            InitializeComponent();
            AddEventsCheckBtn();
            AddEventsBtn();
        }

        private void CreateClient()
        {
            string? error_msg = client.Connect();
            if (error_msg != null)
            {
                MessageBox.Show(error_msg);
                Close();
            }
        }

        public void ShowMsgBox(string msg)
        {
            MessageBox.Show(msg);
        }

        private void AddEventsCheckBtn()
        {
            CheckBtn check_btn = new CheckBtn(this);
            ColdWaterCB.Unchecked += check_btn.ColdWaterDeviceDisable;
            HotWaterCB.Unchecked += check_btn.HotWaterDeviceDisable;
            EnergyCB.Unchecked += check_btn.EnergyDeviceDisable;
            ColdWaterCB.Checked += check_btn.ColdWaterDeviceEnable;
            HotWaterCB.Checked += check_btn.HotWaterDeviceEnable;
            EnergyCB.Checked += check_btn.EnergyDeviceEnable;
        }

        private void AddEventsBtn()
        {
            Btn btn = new Btn(this);
            SumbitBtn.Click += btn.Sumbit;
        }
    }
}
