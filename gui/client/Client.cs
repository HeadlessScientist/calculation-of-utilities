using System.Net.Sockets;
using System;
using System.IO;
using System.Text;
using System.Text.Json;
using System.Net;
using System.Threading.Tasks;
using dataclasses;

namespace client
{
    public class TCPClient
    {
        private string ip;
        private int port;
        private TcpClient listener = new TcpClient();

        public TCPClient(string ip = "127.0.0.1", int port = 8080)
        {
            this.ip = ip;
            this.port = port;
        }

        public string Read()
        {
            byte[] data = new byte[1024];
            var stream = listener.GetStream();
            int bytes = stream.Read(data);
            return Encoding.UTF8.GetString(data, 0, bytes);
        }

        public void Send(string msg)
        {
            byte[] requestData = Encoding.UTF8.GetBytes(msg);
            var stream = listener.GetStream();
            stream.Write(requestData);
        }

        public string? Connect()
        {
            try
            {
                listener.Connect(ip, port);
                return null;
            } catch (Exception e) {
                return $"{e}";
            }
            
        }
    }
}