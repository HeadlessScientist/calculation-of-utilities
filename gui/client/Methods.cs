using System;
using System.Text.Json;
using gui;
using dataclasses;

namespace client
{
    static class Methods
    {
        static public string CalculatePrice(MainWindow win)
        {
            Method request = new Method(
                "CalculatePrice",
                GetDataCalculatePrice(win)
            );
            return JsonSerializer.Serialize(request);
        }

        static private string GetDataCalculatePrice(MainWindow win)
        {
            Divices divices;
            DiviceData cold_water;
            DiviceData hot_water;
            DiviceData energy_day;
            DiviceData energy_night;

            divices = new Divices(
                Convert.ToBoolean(win.ColdWaterCB.IsChecked),
                Convert.ToBoolean(win.HotWaterCB.IsChecked),
                Convert.ToBoolean(win.EnergyCB.IsChecked)
            );

            if (divices.cold_water) {
                cold_water = new DiviceData(
                    Convert.ToSingle(win.ColdWaterStartTB.Text),
                    Convert.ToSingle(win.ColdWaterCurrentTB.Text)
                );
            } else {
                cold_water = new DiviceData();
            }
            
            if (divices.hot_water) {
                hot_water = new DiviceData(
                    Convert.ToSingle(win.HotWaterStartTB.Text),
                    Convert.ToSingle(win.HotWaterCurrentTB.Text)
                );
            } else {
                hot_water = new DiviceData();
            }

            if (divices.energy) {
                energy_day = new DiviceData(
                    Convert.ToSingle(win.EnergyDayStartTB.Text),
                    Convert.ToSingle(win.EnergyDayCurrentTB.Text)
                );
                energy_night = new DiviceData(
                    Convert.ToSingle(win.EnergyNightStartTB.Text),
                    Convert.ToSingle(win.EnergyNightCurrentTB.Text)
                );
            } else {
                energy_day = new DiviceData();
                energy_night = new DiviceData();
            }

            Tariff tariff = new Tariff();

            Standart standart = new Standart();

            GetDiviceData data = new GetDiviceData(
                divices,
                Convert.ToInt32(win.HumanTB.Text),
                cold_water,
                hot_water,
                energy_day,
                energy_night,
                win.config.config.tariff,
                win.config.config.standart
            );
            return JsonSerializer.Serialize<GetDiviceData>(data);
        }
    }
}