namespace dataclasses
{
    public class DataConfig
    {
        public Tariff tariff {get; set;}
        public Standart standart {get; set;}

        public DataConfig(
            Tariff tariff,
            Standart standart
        )
        {
            this.tariff = tariff;
            this.standart = standart;
        }
    }
    
    public class Method
    {
        public string method {get; set;}
        public string param {get; set;}

        public Method(string method, string param)
        {
            this.method = method;
            this.param = param;
        }
    }

    public class Divices
    {
        public bool cold_water {get; set;}
        public bool hot_water {get; set;}
        public bool energy {get; set;}

        public Divices(bool cold_water, bool hot_water, bool energy)
        {
            this.cold_water = cold_water;
            this.hot_water = hot_water;
            this.energy = energy;
        }
    }

    public class DiviceData
    {
        public float start {get; set;}
        public float current {get; set;}

        public DiviceData(float start = 0f, float current = 0f)
        {
            this.start = start;
            this.current = current;
        }
    }

    public class Tariff
    {
        public float cold_water {get; set;}
        public float energy {get; set;}
        public float energy_day {get; set;}
        public float energy_night {get; set;}
        public float hot_water {get; set;}
        public float hot_water_energy {get; set;}

        public Tariff(
            float cold_water = 35.78f,
            float energy = 4.28f,
            float energy_day = 4.9f,
            float energy_night = 2.31f,
            float hot_water = 35.78f,
            float hot_water_energy = 998.69f
            )
        {
            this.cold_water = cold_water;
            this.energy = energy;
            this.energy_day = energy_day;
            this.energy_night = energy_night;
            this.hot_water = hot_water;
            this.hot_water_energy = hot_water_energy;
        }
    }

    public class Standart
    {
        public float cold_water {get; set;}
        public float hot_water {get; set;}
        public float hot_water_energy {get; set;}
        public float energy {get; set;}

        public Standart(
            float cold_water = 4.85f,
            float hot_water = 4.01f,
            float hot_water_energy = 0.05349f,
            float energy = 164f
            )
        {
            this.cold_water = cold_water;
            this.energy = energy;
            this.hot_water = hot_water;
            this.hot_water_energy = hot_water_energy;
        }
    }

    public class GetDiviceData
    {
        public Divices check_divices {get; set;}
        public int human {get; set;}
        public DiviceData cold_water {get; set;}
        public DiviceData hot_water {get; set;}
        public DiviceData energy_day {get; set;}
        public DiviceData energy_night {get; set;}
        public Tariff tariff {get; set;}
        public Standart standart {get; set;}

        public GetDiviceData(
            Divices check_divices,
            int human,
            DiviceData cold_water,
            DiviceData hot_water,
            DiviceData energy_day,
            DiviceData energy_night,
            Tariff tariff,
            Standart standart
            )
        {
            this.check_divices = check_divices;
            this.human = human;
            this.cold_water = cold_water;
            this.hot_water = hot_water;
            this.energy_day = energy_day;
            this.energy_night = energy_night;
            this.tariff = tariff;
            this.standart = standart;
        }
    }

    public class ResponseCalculatePrice
    {
        public float cold_water {get; set;}
        public float hot_water {get; set;}
        public float hot_water_energy {get; set;}
        public float energy {get; set;}
        public float energy_day {get; set;}
        public float energy_night {get; set;}
        public float final_price {get; set;}

        public ResponseCalculatePrice(
            float cold_water,
            float hot_water,
            float hot_water_energy,
            float final_price,
            float energy = 0f,
            float energy_day = 0f,
            float energy_night = 0f
            )
        {
            this.cold_water = cold_water;
            this.energy = energy;
            this.hot_water = hot_water;
            this.hot_water_energy = hot_water_energy;
            this.energy_day = energy_day;
            this.energy_night = energy_night;
            this.final_price = final_price;
        }
    }
}