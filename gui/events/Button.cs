using System;
using System.Windows;
using client;
using dataclasses;

namespace gui
{
    public class Btn
    {
        private MainWindow win;

        public Btn(MainWindow window)
        {
            win = window;
        }

        public void Sumbit(object sender, RoutedEventArgs e)
        {
            if (Validators.CheckFieldCorrect(win.Grid))
            {
                win.client.Send(Methods.CalculatePrice(win));
                string response = win.client.Read();
                TB.PrintCalc(win, response);
            }
        }
    }
}