using System.Windows;

namespace gui
{
    public class CheckBtn
    {
        private MainWindow win;

        public CheckBtn(MainWindow window)
        {
            win = window;
        }

        public void ColdWaterDeviceDisable(object sender, RoutedEventArgs e)
        {
            win.ColdWaterStartTB.IsEnabled = false;
            win.ColdWaterCurrentTB.IsEnabled = false;
        }

        public void ColdWaterDeviceEnable(object sender, RoutedEventArgs e)
        {
            win.ColdWaterStartTB.IsEnabled = true;
            win.ColdWaterCurrentTB.IsEnabled = true;
        }

        public void HotWaterDeviceDisable(object sender, RoutedEventArgs e)
        {
            win.HotWaterStartTB.IsEnabled = false;
            win.HotWaterCurrentTB.IsEnabled = false;
        }

        public void HotWaterDeviceEnable(object sender, RoutedEventArgs e)
        {
            win.HotWaterStartTB.IsEnabled = true;
            win.HotWaterCurrentTB.IsEnabled = true;
        }

        public void EnergyDeviceDisable(object sender, RoutedEventArgs e)
        {
            win.EnergyDayStartTB.IsEnabled = false;
            win.EnergyDayCurrentTB.IsEnabled = false;
            win.EnergyNightStartTB.IsEnabled = false;
            win.EnergyNightCurrentTB.IsEnabled = false;

            win.EnergyDayCalcLB.Visibility = Visibility.Collapsed;
            win.EnergyDayCalcNumLB.Visibility = Visibility.Collapsed;
            win.EnergyDaySymbolLB.Visibility = Visibility.Collapsed;

            win.EnergyCalcLB.Visibility = Visibility.Visible;
            win.EnergyCalcNumLB.Visibility = Visibility.Visible;
            win.EnergySymbolLB.Visibility = Visibility.Visible;

            win.EnergyNightCalcLB.Visibility = Visibility.Collapsed;
            win.EnergyNightCalcNumLB.Visibility = Visibility.Collapsed;
            win.EnergyNightSymbolLB.Visibility = Visibility.Collapsed;
        }

        public void EnergyDeviceEnable(object sender, RoutedEventArgs e)
        {
            win.EnergyDayStartTB.IsEnabled = true;
            win.EnergyDayCurrentTB.IsEnabled = true;
            win.EnergyNightStartTB.IsEnabled = true;
            win.EnergyNightCurrentTB.IsEnabled = true;

            win.EnergyDayCalcLB.Visibility = Visibility.Visible;
            win.EnergyDayCalcNumLB.Visibility = Visibility.Visible;
            win.EnergyDaySymbolLB.Visibility = Visibility.Visible;

            win.EnergyCalcLB.Visibility = Visibility.Collapsed;
            win.EnergyCalcNumLB.Visibility = Visibility.Collapsed;
            win.EnergySymbolLB.Visibility = Visibility.Collapsed;

            win.EnergyNightCalcLB.Visibility = Visibility.Visible;
            win.EnergyNightCalcNumLB.Visibility = Visibility.Visible;
            win.EnergyNightSymbolLB.Visibility = Visibility.Visible;
        }
    }
}