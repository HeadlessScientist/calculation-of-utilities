using System.Windows;
using System.Text.Json;
using client;
using dataclasses;

namespace gui
{
    public class TB
    {
        private MainWindow win;

        public TB(MainWindow window)
        {
            win = window;
        }

        static public void PrintCalc(MainWindow win, string data)
        {
            ResponseCalculatePrice resp = JsonSerializer.Deserialize<ResponseCalculatePrice>(data);

            win.ColdWaterCalcNumLB.Text = $"{resp.cold_water:C2}";
            win.HotWaterEnergyCalcNumLB.Text = $"{resp.hot_water_energy:C2}";
            win.HotWaterCalcNumLB.Text = $"{resp.hot_water:C2}";
            win.EnergyDayCalcNumLB.Text = $"{resp.energy_day:C2}";
            win.EnergyCalcNumLB.Text = $"{resp.energy:C2}";
            win.EnergyNightCalcNumLB.Text = $"{resp.energy_night:C2}";
            win.ResultCalcNumLB.Text = $"{resp.final_price:C2}";
        }
    }
}