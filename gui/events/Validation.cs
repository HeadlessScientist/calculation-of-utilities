using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace gui
{
    static public class Validators
    {
        static public bool CheckFieldCorrect(Grid grid)
        {
            TextBox tb;
            bool check = true;

            foreach (FrameworkElement el in grid.Children)
            {
                if (el is TextBox)
                {
                    tb = (TextBox) el;
                    if (!IsEnabled(tb) || (IsNotEmerty(tb) && IsNotMinus(tb)))
                    {
                        tb.BorderBrush = Brushes.Black;
                        continue;
                    }
                    check = false;
                    tb.BorderBrush = Brushes.Red;
                }
            }
            return check;
        }

        private static bool IsEnabled(TextBox field)
        {
            return field.IsEnabled;
        }

        private static bool IsNotEmerty(TextBox field)
        {
            return field.Text != "";
        }

        private static bool IsNotMinus(TextBox field)
        {
            try
            {
                return Convert.ToSingle(field.Text) >= 0f;
            } catch {
                return false;
            }
        }
    }
}