using System.IO;
using System.Text;
using System.Text.Json;
using System.Windows.Controls;
using dataclasses;
using gui;

namespace config
{
    public class Config
    {
        public DataConfig config;
        public Config(){
            using (FileStream fs = new FileStream("static/config.json", FileMode.Open))
            {
                config = JsonSerializer.Deserialize<DataConfig>(fs);
            }
        }
    }
}